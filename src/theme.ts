import { createTheme } from "@fluentui/react";

export const theme = createTheme({
  palette: {
    themePrimary: "#90eb63",
    themeLighterAlt: "#060904",
    themeLighter: "#172610",
    themeLight: "#2b461e",
    themeTertiary: "#568d3b",
    themeSecondary: "#7fce57",
    themeDarkAlt: "#9aed71",
    themeDark: "#a9ef86",
    themeDarker: "#bff4a4",
    neutralLighterAlt: "#303030",
    neutralLighter: "#383838",
    neutralLight: "#464646",
    neutralQuaternaryAlt: "#4e4e4e",
    neutralQuaternary: "#555555",
    neutralTertiaryAlt: "#727272",
    neutralTertiary: "#c8c8c8",
    neutralSecondary: "#d0d0d0",
    neutralSecondaryAlt: "#d0d0d0",
    neutralPrimaryAlt: "#dadada",
    neutralPrimary: "#ffffff",
    neutralDark: "#f4f4f4",
    black: "#f8f8f8",
    white: "#262626",
  },
});
