import { AdsrState } from '../../features/adsr/adsr-slice';
import { playNote } from '../../features/keyboard/keyboard-slice';

const playNoteMiddleware = (store: any) => (next: any) => (action: any) => {
    if (action.type === playNote.type) {
        const state = store.getState();
        const { adsr } = state;
        const note = action.payload;

        generateNote(adsr, note);
    }

    return next(action);
};

const generateNote = (adsr: AdsrState, note: string) => {
    if (!note) return;

    const attack = adsr.attack / 1000;
    const decay = adsr.decay / 1000;
    const sustain = adsr.sustain / 1000;
    const release = adsr.release / 1000;

    const audioContext = new AudioContext();
    const oscillator = audioContext.createOscillator();
    const gain = audioContext.createGain();

    oscillator.connect(gain);
    gain.connect(audioContext.destination);
    oscillator.type = 'sine';
    oscillator.frequency.value = translateNoteToFrequency(note);

    gain.gain.setValueAtTime(0, audioContext.currentTime);
    gain.gain.linearRampToValueAtTime(1, audioContext.currentTime + attack);
    gain.gain.linearRampToValueAtTime(sustain, audioContext.currentTime + attack + decay);
    gain.gain.linearRampToValueAtTime(0, audioContext.currentTime + attack + decay + release);
    
    oscillator.start();
    oscillator.stop(audioContext.currentTime + attack + decay + release);
}

const translateNoteToFrequency = (note: string) => {
    const frequencyMap: Record<string, number> = {
        C: 130.81,
        D: 146.83,
        E: 164.81,
        F: 174.61,
        G: 196.00,
        A: 220.00,
        B: 246.94,
    }

    return frequencyMap[note];
}


export default playNoteMiddleware;