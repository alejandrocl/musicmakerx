import { configureStore } from "@reduxjs/toolkit";
import adsrReducer from "../features/adsr/adsr-slice";
import keyboardReducer from "../features/keyboard/keyboard-slice";
import playNoteMiddleware from "./middleware/play-note-middleware";

export const store = configureStore({
    reducer: {
        adsr: adsrReducer,
        keyboard: keyboardReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(playNoteMiddleware),

});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
