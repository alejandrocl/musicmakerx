import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface AdsrState {
  attack: number;
  decay: number;
  sustain: number;
  release: number;
}

const initialState: AdsrState = {
  attack: 100,
  decay: 100,
  sustain: 100,
  release: 100,
};

const adsrSlice = createSlice({
  name: "adsr",
  initialState,
  reducers: {
    setADSR(state, action: PayloadAction<AdsrState>) {
      state.attack = action.payload.attack;
      state.decay = action.payload.decay;
      state.sustain = action.payload.sustain;
      state.release = action.payload.release;
    },
    setAttack(state, action: PayloadAction<number>) {
      state.attack = action.payload;
    },
    setDecay(state, action: PayloadAction<number>) {
      state.decay = action.payload;
    },
    setSustain(state, action: PayloadAction<number>) {
      state.sustain = action.payload;
    },
    setRelease(state, action: PayloadAction<number>) {
      state.release = action.payload;
    },
  },
});

export const { setADSR, setAttack, setDecay, setSustain, setRelease } =
  adsrSlice.actions;
export default adsrSlice.reducer;
