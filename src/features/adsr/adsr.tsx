import React from "react";
import { Slider, Stack } from "@fluentui/react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../app/store";
import { setAttack, setDecay, setRelease, setSustain } from "./adsr-slice";

const MIN_ADSR_VALUE = 0;
const MAX_ADSR_VALUE = 1000;
const ADSR_STEP = 1;

const ADSR: React.FC = () => {
  const dispatch = useDispatch();
  const { attack, decay, sustain, release } = useSelector(
    (state: RootState) => state.adsr
  );

  const handleSliderChange = (value: number, sliderName: string) => {
    switch (sliderName) {
      case "attack":
        dispatch(setAttack(value));
        break;
      case "decay":
        dispatch(setDecay(value));
        break;
      case "sustain":
        dispatch(setSustain(value));
        break;
      case "release":
        dispatch(setRelease(value));
        break;
      default:
        break;
    }
  };

  return (
    <Stack horizontal>
      <Slider
        label="Attack"
        min={MIN_ADSR_VALUE}
        max={MAX_ADSR_VALUE}
        step={ADSR_STEP}
        value={attack}
        vertical
        onChange={(value) => handleSliderChange(value, "attack")}
      />

      <Slider
        label="Decay"
        min={MIN_ADSR_VALUE}
        max={MAX_ADSR_VALUE}
        step={ADSR_STEP}
        value={decay}
        vertical
        onChange={(value) => handleSliderChange(value, "decay")}
      />

      <Slider
        label="Sustain"
        min={MIN_ADSR_VALUE}
        max={MAX_ADSR_VALUE}
        value={sustain}
        vertical
        onChange={(value) => handleSliderChange(value, "sustain")}
      />

      <Slider
        label="Release"
        min={MIN_ADSR_VALUE}
        max={MAX_ADSR_VALUE}
        step={ADSR_STEP}
        value={release}
        vertical
        onChange={(value) => handleSliderChange(value, "release")}
      />
    </Stack>
  );
};

export default ADSR;
