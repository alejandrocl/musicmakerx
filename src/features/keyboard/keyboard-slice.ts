import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface KeyboardState {
    note: string | null;
}

const initialState: KeyboardState = {
    note: null,
};

const keyboardSlice = createSlice({
    name: 'keyboard',
    initialState,
    reducers: {
        playNote: (state, action: PayloadAction<string>) => {
            state.note = action.payload;
        },
    },
});

export const { playNote } = keyboardSlice.actions;
export default keyboardSlice.reducer;