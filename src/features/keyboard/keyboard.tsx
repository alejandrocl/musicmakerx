import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { PrimaryButton, Stack } from '@fluentui/react';
import { playNote } from './keyboard-slice';

const notes = ['C', 'D', 'E', 'F', 'G', 'A', 'B'];
const keyboardKeys = ["A", "S", "D", "F", "G", "H", "J", "K"];

const getNoteForKey = (key: string): string | undefined => {
    const index = keyboardKeys.indexOf(key.toUpperCase());
    if (index !== -1) {
      return notes[index];
    }
    return undefined;
  };

const Keyboard: React.FC = () => {

    const dispatch = useDispatch();

    const handleNoteClick = (note: string) => {
        dispatch(playNote(note));
    };

    const handleKeyDown = (event: any) => {
        const note = getNoteForKey(event.key);
        if (note) {
          dispatch(playNote(note));
        }
      };
    
      useEffect(() => {
        document.addEventListener("keydown", handleKeyDown);
        return () => {
          document.removeEventListener("keydown", handleKeyDown);
        };
      });

    return (
        <Stack horizontal tokens={{ childrenGap: 10 }}>
            {notes.map((note, index) => (
                <PrimaryButton key={index} className="key" onClick={() => handleNoteClick(note)}>
                    {note}
                </PrimaryButton>
            ))}
        </Stack>
    );
};

export default Keyboard;