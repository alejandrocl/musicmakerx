import { FontWeights, IStackStyles, IStackTokens, ITextStyles, Stack, Text } from '@fluentui/react';
import React from 'react';
import './App.css';
import ADSR from './features/adsr/adsr';
import Keyboard from './features/keyboard/keyboard';

const boldStyle: Partial<ITextStyles> = { root: { fontWeight: FontWeights.semibold } };
const stackTokens: IStackTokens = { childrenGap: 15 };
const stackStyles: Partial<IStackStyles> = {
  root: {
    width: '100vw',
    height: '100vh',
    margin: '0 auto',
    textAlign: 'center',
    gridGap: '2rem',
  },
};

export const App: React.FunctionComponent = () => {
  return (
    <Stack horizontalAlign="center" verticalAlign="center" verticalFill styles={stackStyles} tokens={stackTokens}>
      <Text variant="xxLarge" styles={boldStyle}>
        Music Maker X
      </Text>
      <ADSR />
      <Keyboard />
    </Stack>
  );
};
